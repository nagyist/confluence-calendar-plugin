// Show the debug window
function showDebug() {
  window.top.debugWindow =
      window.open("",
                  "Debug",
                  "left=0,top=0,width=300,height=700,scrollbars=yes,"
                  +"status=yes,resizable=yes");
  window.top.debugWindow.opener = self;
  // open the document for writing
  window.top.debugWindow.document.open();
  window.top.debugWindow.document.write(
      "<HTML><HEAD><TITLE>Debug Window</TITLE></HEAD><BODY><PRE>\n");
}


// If the debug window exists, then write to it
function debug(text, value, recurse) {
  if (window.top.debugWindow && ! window.top.debugWindow.closed) {
    if (value)
      text += describeObject(value, recurse ? recurse : 1);

    window.top.debugWindow.document.write(text+"\n");
  }
}

function describeObject(obj, recurse, indent, described)
{
  indent = (indent ? indent : "");
  if (!described) described = new Array();

  var myIndent = (indent ? indent : "") + "    ";

  if (obj)
  {
    var type = typeof(obj);
    var desc = "(" + typeof(obj) + ") ";

    if (recurse > 0 && type == "object" && !_containsValue(described, obj)) {
      described.push(obj);

   	  desc += "{\n";

      for (prop in obj)
      {
        desc += myIndent + prop + " = ";
        try {
          desc += describeObject(obj[prop], recurse-1, myIndent, described);
        } catch (e) {
          desc += "!" + e + "!";
        }
        desc += "\n";
      }

      desc += indent + "}\n";
    } else if (type == "function") {
      // Do nothing...
    } else if (type == "string") {
      desc += "\"" + obj + "\"";
    } else {
      desc += obj;
    }
  }
  else
    desc = "null";

  return desc;
}

function _containsValue(values, value)
{
	if (values)
		for (var i = 0; i < values.length; i++)
			if (values[i] == value)
				return true;
	return false;
}


// If the debug window exists, then close it
function hideDebug() {
  if (window.top.debugWindow && ! window.top.debugWindow.closed) {
    window.top.debugWindow.close();
    window.top.debugWindow = null;
  }
}
