package com.atlassian.confluence.extra.cal2.mgr;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTimeZone;
import org.joda.time.Interval;

import com.atlassian.confluence.extra.cal2.model.ICalCalendar;
import com.atlassian.confluence.extra.cal2.model.ICalendar;
import com.atlassian.confluence.extra.cal2.model.IEvent;


/**
 * Manages a collection of references to calendar holders that is displayed 
 * in an instance of the macro
 * 
 * @author ndwyer
 *
 */
public class CalendarGroup implements ICalendar
{
    private CalendarGroupKey hashKey;

    // collection of CalendarHolder ids/references
    private List<String> members;
    
    /**
     * A list of the children that should not be displayed
     */
    private List<String> inactive;

    private String suppressSubscribeLink;

    public CalendarGroup(CalendarGroupKey hashKey)
    {
        this.hashKey = hashKey;
        
        // setup calendar holder references
        members = new ArrayList<String>();
        inactive = new ArrayList<String>();
    }

    public String getId()
    {
        return hashKey.getGroupId();
    }

    public CalendarGroupKey getHashKey()
    {
        return hashKey;
    }
    
    
    // get calendar ids
    public List<String> getMembers()
    {
        return members;
    }
    
    // look up list of calendar holders
    public List<ICalendar> getHolders()
    {
    	return CalendarManager.getInstance().getHoldersForGroup(getHashKey());
    }
    
    // TODO  TEMP Velocity macros expect this method
    public List<ICalendar> getDescendents()
    {
        return getHolders();
    }

    // add id (don't allow duplicates)
    public boolean add(String id)
    {
        return (!contains(id) && members.add(id));
    }
    
    // remove id
    public boolean remove(String id)
    {
        return members.remove(id);
    }
    
    // contains id
    public boolean contains(String id)
    {
        return members.contains(id);
    }

    
    
	public void commit() throws CalendarException
	{
		// TODO Auto-generated method stub
		
	}

	public IEvent findBaseEvent(String id) throws CalendarException
	{
        Iterator i = getHolders().iterator();
        IEvent event = null;
        ICalendar cal;

        while ( i.hasNext() && event == null ) {
            try {
                cal = ( ICalendar ) i.next();
                if ( !isUndisplayable(cal)  ) {
                    event = cal.findBaseEvent( id );
                }
            } catch ( CalendarException e ) {
                e.printStackTrace();
            }
        }

        return event;
	}

	public IEvent findEvent(String id) throws CalendarException
	{
        Iterator i = getHolders().iterator();
        IEvent event = null;
        ICalendar cal;

        while ( i.hasNext() && event == null ) {
            try {
                cal = ( ICalendar ) i.next();
                if ( !isUndisplayable(cal) ) {
                    event = cal.findEvent( id );
                }
            } catch ( CalendarException e ) {
                e.printStackTrace();
            }
        }

        return event;	}

	public List findEvents(Interval interval, DateTimeZone targetTimeZone) throws CalendarException
	{
        List events = new LinkedList();
        Iterator i = getHolders().iterator();
        ICalendar cal;

        while ( i.hasNext() ) {
            cal = ( ICalendar ) i.next();
            try {
                if ( !isUndisplayable(cal) ) {
                    events.addAll( cal.findEvents( interval, targetTimeZone ) );
                }
            } catch ( CalendarException e ) {
                e.printStackTrace();
            }
        }

        return events;
	}

	public String getColor()
	{
		// TODO Auto-generated method stub
		return null;
	}

    public void setSuppressSubscribeLink(String suppressSubscribeLink) {
        this.suppressSubscribeLink = suppressSubscribeLink;
    }

    public String getSuppressSubscribeLink() {
        return suppressSubscribeLink;
    }

    public String getDescription()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public String getName()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public void init() throws CalendarException
	{
		// TODO Auto-generated method stub
		
	}

	public boolean isReadOnly()
	{
		// TODO Auto-generated method stub
		return false;
	}

	public void setDescription(String description)
	{
		// TODO Auto-generated method stub
		
	}

	public void setName(String name)
	{
		// TODO Auto-generated method stub
		
	}

	public void addChild(ICalCalendar calendar)
	{
		CalendarManager.getInstance().addCalendarHolder(calendar);
		add(calendar.getId());
	}

	public boolean removeDescendent(ICalendar cal)
	{
		String calId = cal.getId();
		
        CalendarManager.getInstance().removeCalendarHolder(calId);
		return remove(calId);
	}

	
    public boolean isUndisplayable(ICalendar cal)
    {
        return inactive.contains(cal.getId());
    }

    public void clearUndisplayable(ICalendar cal)
    {
        inactive.remove(cal.getId());        
    }

    public void setUndisplayable(ICalendar cal)
    {
        inactive.add(cal.getId());        
    }
    
    
    // TODO modify member order
    
    // serialization
}
