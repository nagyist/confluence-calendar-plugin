package com.atlassian.confluence.extra.cal2.mgr;

public class CalendarGroupKey
{
    public static final String KEY_SEPARATOR = ":";
    
    private String spaceKey;
    private String pageKey;
    private String groupId;
    
    private String keyValue;
    
    public CalendarGroupKey(String spaceKey, String pageKey, String groupKey)
    {
    	this.spaceKey = spaceKey;
    	this.pageKey = pageKey;
    	this.groupId = groupKey;
    	
        this.keyValue = spaceKey + KEY_SEPARATOR + pageKey + KEY_SEPARATOR + groupKey;
    }
    
    public String getValue() 
    {
        return this.keyValue;
    }
    
    
    public String getSpaceKey()
	{
		return spaceKey;
	}

	public String getPageKey()
	{
		return pageKey;
	}

	public String getGroupId()
	{
		return groupId;
	}

	
	@Override
    public boolean equals(Object obj)
    {
        if (obj instanceof CalendarGroupKey)
        {
            CalendarGroupKey that = (CalendarGroupKey) obj;
            return (this == that) || that.keyValue.equals( this.keyValue);
        }
        
        return false;
    }
    
    @Override
    public int hashCode()
    {
        return this.keyValue.hashCode();
    }
}
