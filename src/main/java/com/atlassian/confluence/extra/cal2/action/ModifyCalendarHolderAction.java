package com.atlassian.confluence.extra.cal2.action;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.SortedMap;
import java.util.regex.Pattern;

import org.randombits.source.Source;
import org.randombits.source.StringSource;
import org.randombits.source.URLSource;
import org.randombits.source.confluence.AttachmentSource;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.cal2.mgr.CalendarException;
import com.atlassian.confluence.extra.cal2.mgr.CalendarGroup;
import com.atlassian.confluence.extra.cal2.model.ICalCalendar;
import com.atlassian.confluence.extra.cal2.model.ICalendar;
import com.atlassian.confluence.links.linktypes.AttachmentLink;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.renderer.links.Link;
import com.atlassian.renderer.links.LinkResolver;
import com.opensymphony.util.TextUtils;

public class ModifyCalendarHolderAction extends CalendarHolderAction
{

	private static final Pattern HASHED_PASSWORD = Pattern.compile("\\*+");

	private LinkResolver linkResolver;

	public ModifyCalendarHolderAction()
	{
		setRequireEditPermission();
	}

	@Override
	public String execute() throws Exception
	{
		ICalCalendar cal = getSubCalendar();
		if (cal != null)
		{
			setName(cal.getName());
			setDescription(cal.getDescription());
			setColor(cal.getColor());

			Source src = cal.getSource();
			if (src instanceof URLSource)
			{
				URLSource urlSrc = (URLSource) src;
				setSourceType(REMOTE_SOURCE);
				setRemoteSourceUrl(urlSrc.getUrl().toExternalForm());
				setConnectionTimeout(Integer.toString((int) urlSrc.getConnectionTimeout() / 1000));
				remoteSourceUsername = urlSrc.getUsername();
				remoteSourceHasPassword = urlSrc.hasPassword();
				Charset encoding = urlSrc.getEncoding();
				remoteSourceEncoding = encoding == null ? null : encoding.name();
			} else if (src instanceof AttachmentSource)
			{
				AttachmentSource attSrc = (AttachmentSource) src;
				setSourceType(ATTACHMENT_SOURCE);
				long contentId = attSrc.getContentId();
				attachmentLink = "";

				if (getContent() != null && contentId != getContent().getId())
				{
					ContentEntityObject content = getContentEntityManager().getById(contentId);
					if (content instanceof Page)
					{
						attachmentLink = content.getTitle();
					} else if (content instanceof BlogPost)
					{
						attachmentLink = ((BlogPost) content).getDatePath() + content.getTitle();
					}
				}

				attachmentLink += "^" + attSrc.getFileName();
			} else
			{
				setSourceType(LOCAL_SOURCE);
			}
		} else
		{
			setSourceType(LOCAL_SOURCE);
		}

		return INPUT;
	}

    public String addCalendar() {
        if ( !isEditPermitted() )
            return PAGE_NOT_PERMITTED;

// TODO TEMP
//        if ( calendarClassname == null ) {
//            List handlers = getCalendarManager().getHandlers();
//            switch ( handlers.size() ) {
//                case 0:
//                    addActionError( "Unable to find any calendar types to add." );
//                    return ERROR;
//                case 1: // there is only one handler - skip the selection page.
//                    calendarClassname = ( ( CalendarHandler ) handlers.get( 0 ) ).getCalendarClass().getName();
//                    return doAdd();
//                default:
                    return INPUT;
//            }
//        }
//
//        return doAdd();
    }
    
    public String doAdd() {
        ICalCalendar calendar = new ICalCalendar();
        setSubCalendar( calendar );

        try {
            calendar.init();
        } catch ( CalendarException e ) {
            e.printStackTrace();
            addActionError( getText( "calendar.error.ical.calendarException", e.getMessage() ) );
            return ERROR;
        }

        String name = getName();
        if ( name == null || name.trim().length() == 0 ) {
            addActionError( getText( "calendar.error.ical.nameMissing" ) );
            return ERROR;
        }

        String result = updateCalendar( calendar, name );

        if ( result != null )
            return result;

        getCalendar().addChild( calendar );
        return saveCalendar();
    }
	
    public String deleteCalendar() {
        if ( !isEditPermitted() )
            return PAGE_NOT_PERMITTED;

        ICalendar cal = getSubCalendar();
        CalendarGroup group = getCalendar();

        if ( group != null && cal != null ) {
            if ( group.removeDescendent( cal ) ) {
                return saveCalendar();
            }
        }

        addActionError( "Unable to delete the calendar." );
        return ERROR;
    }

    private String updateCalendar( ICalCalendar calendar, String name ) {
        calendar.setName( name );

        if ( description != null && description.trim().length() > 0 )
            calendar.setDescription( description );
        else
            calendar.setDescription( null );

        if ( color != null && color.trim().length() > 0 )
            calendar.setColor( color );
        else
            calendar.setColor( null );

        return doSource( calendar );
    }



    private String doSource( ICalCalendar calendar ) {
        Source src = calendar.getSource();

        if ( LOCAL_SOURCE.equals( sourceType ) ) {
            if ( src == null )
                src = new StringSource();
            else if ( !( src instanceof StringSource ) ) {
                addActionError( getText( "calendar.error.ical.source.typeMismatch" ) );
                return ERROR;
            }
        } else if ( REMOTE_SOURCE.equals( sourceType ) ) {
            URLSource urlSrc;
            if ( src == null ) {
                urlSrc = new URLSource();
                urlSrc.setConnectionTimeout( DEFAULT_TIMEOUT * 1000 );
                urlSrc.setReadTimeout( DEFAULT_TIMEOUT * 1000 );
            } else if ( src instanceof URLSource )
                urlSrc = ( URLSource ) src;
            else {
                addActionError( getText( "calendar.error.ical.source.typeMismatch" ) );
                return ERROR;
            }

            if ( !TextUtils.stringSet( remoteSourceUrl ) ) {
                addActionError( getText( "calendar.error.ical.source.remoteUrlMissing" ) );
                return ERROR;
            }

            if ( remoteSourceUrl.startsWith( "webcal" ) )
                remoteSourceUrl = "http" + remoteSourceUrl.substring( 6 );          
            
            URL remoteSourceUrlObj = null;
            try {
                remoteSourceUrlObj = new URL ( remoteSourceUrl );
                urlSrc.setUrl( remoteSourceUrlObj );
            } catch ( MalformedURLException e ) {
                addActionError( e.getMessage() );
                return ERROR;
            }

            // CAL-247: don't allow a calendar to subscribe to an ical
            // calendar generated on the same Confluence page.
            Settings settings = settingsManager.getGlobalSettings();            
            if ( remoteSourceUrl.startsWith( settings.getBaseUrl() ) ) {
                // is the URL coming from the current page?
                if ( remoteSourceUrlObj.getPath().equals( getPage().getUrlPath() ) ) {
                    addActionError( getText( "calendar.error.ical.source.urlFromSamePage" ) );
                    return ERROR;        					                    
                }

                String urlQuery = remoteSourceUrlObj.getQuery();
                String[] queryParams = urlQuery.split( "&" );
                for ( int i = 0; i < queryParams.length; i++ ) {
                    if ( queryParams[i].startsWith( "pageId" ) ) {
                        String remotePageId = queryParams[i].substring( 7 );
                        if ( remotePageId.equals( String.valueOf( getPageId() ) ) ) {
                            addActionError( getText( "calendar.error.ical.source.urlFromSamePage" ) );
                            return ERROR;        					
                        }
                    }
                }
            }            
            
            try {
                int timeout = Integer.parseInt( connectionTimeout );
                urlSrc.setConnectionTimeout( timeout * 1000 );
                urlSrc.setReadTimeout( timeout * 1000 );
            } catch ( IllegalArgumentException iae ) {
                addActionError( iae.getMessage() );
                return ERROR;
            }

            urlSrc.setUsername( nonEmpty( remoteSourceUsername ) );
            if ( remoteSourcePassword == null || !HASHED_PASSWORD.matcher( remoteSourcePassword ).matches() )
                urlSrc.setPassword( nonEmpty( remoteSourcePassword ) );

            urlSrc.setEncoding( getRemoteSourceCharset() );

            src = urlSrc;
        }

        else if ( ATTACHMENT_SOURCE.equals( sourceType ) )

        {
            if ( TextUtils.stringSet( attachmentLink ) ) {
                if ( src == null ) {
                    AttachmentSource attSrc = new AttachmentSource();

                    Link link = linkResolver.createLink( getPage().toPageContext(), attachmentLink );
                    if ( link instanceof AttachmentLink ) {
                        Attachment att = ( ( AttachmentLink ) link ).getAttachment();
                        if ( !permissionManager.hasPermission( getRemoteUser(), Permission.VIEW, att ) ) {
                            addActionError( getText( "calendar.error.ical.source.attachmentLinkInaccessible" ) );
                            return ERROR;
                        }
                        attSrc.setContentId( att.getContent().getId() );
                        attSrc.setFileName( att.getFileName() );
                    } else {
                        addActionError( getText( "calendar.error.ical.source.attachmentLinkNotAttachment" ) );
                        return ERROR;
                    }
                    src = attSrc;
                } else if ( !( src instanceof AttachmentSource ) ) {
                    addActionError( getText( "calendar.error.ical.source.typeMismatch" ) );
                    return ERROR;
                }
            } else {
                addActionError( getText( "calendar.error.ical.source.attachmentLinkMissing" ) );
                return ERROR;
            }
        }

        else

        {
            addActionError( getText( "calendar.error.ical.source.typeMissing" ) );
            return ERROR;
        }

        calendar.setSource( src );

        return null;
    }

	private Charset getRemoteSourceCharset()
	{
		String charsetName = nonEmpty(remoteSourceEncoding);
		if (charsetName != null)
			return Charset.forName(charsetName);

		return null;
	}

	private String nonEmpty(String value)
	{
		if (value != null && value.trim().length() > 0)
			return value;
		return null;
	}

	   public String doEdit() {
	        ICalCalendar calendar = ( ICalCalendar ) getSubCalendar();

	        if ( calendar == null ) {
	            addActionError( getText( "calendar.error.ical.calendarMissing" ) );
	            return ERROR;
	        }

	        String name = getName();
	        if ( name == null || name.trim().length() == 0 ) {
	            addActionError( getText( "calendar.error.ical.nameMissing" ) );
	            return ERROR;
	        }

	        String result = updateCalendar( calendar, name );
	        if ( result != null )
	            return result;

	        return saveCalendar();
	    }


	// protected CalendarHolder createCalendar() {
	// return new DefaultCalendarHolder();
	// }

	public void setLinkResolver(LinkResolver linkResolver)
	{
		this.linkResolver = linkResolver;
	}

	public Collection getAllCharsets()
	{
		SortedMap charsets = Charset.availableCharsets();

		return charsets.entrySet();
	}
}
