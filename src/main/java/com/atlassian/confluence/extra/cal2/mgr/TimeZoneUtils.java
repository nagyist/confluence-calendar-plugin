package com.atlassian.confluence.extra.cal2.mgr;

import java.util.List;

import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalTime;

import com.atlassian.confluence.core.TimeZone;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.user.User;

public class TimeZoneUtils
{
    private static final int MILLIS_PER_SECOND = 1000;

    private static final int SECONDS_PER_MINUTE = 60;

    private static final int MINUTES_PER_HOUR = 60;

    private static final int MILLIS_PER_MINUTE = SECONDS_PER_MINUTE * MILLIS_PER_SECOND;

    private static final int MILLIS_PER_HOUR = MINUTES_PER_HOUR * MILLIS_PER_MINUTE;

    private static final String ETC_REGION = "Etc/";
    

    private static final LocalTime START_TIME_DEFAULT = new LocalTime( 8, 0, 0, 0 );
    
    /**
     * Returns the user's time zone. If the user has not yet specified one, or
     * the user is 'anonymous', the server's default timezone is returned.
     * 
     * @param user
     *            The user to check for.
     * @return The user's timezone.
     */
    public static DateTimeZone getDateTimeZone( TimeZone timeZone ) {
        DateTimeZone zone = null;

        if ( timeZone != null ) {
            String id = timeZone.getID();

            if ( id != null )
                zone = DateTimeZone.forID( id );
        }

        return DateTimeUtils.getZone( zone );
    }
    
    public static String getTimeZoneLabel(DateTimeZone zone)
    {
        if ( zone != null ) {
            long now = DateTimeUtils.getInstantMillis( null );
            String label = zone.getID();
            if ( label.startsWith( ETC_REGION ) )
                label = zone.getShortName( now );
            else {
                label = label.replace( '_', ' ' );
                // label += " (" + getTimeZoneOffset(zone) + ")";
            }

            return label;
        }
        return null;
    }

    public static List getTimeZones()
    {
        // TODO  This list doesn't match the list Joda uses to look up timezone ids
        return com.atlassian.confluence.core.TimeZone.getSortedTimeZones();
    }

    public static String getTimeZoneOffset(DateTimeZone zone)
    {
        int offset = zone.getStandardOffset( DateTimeUtils.getInstantMillis( null ) );
        StringBuffer out = new StringBuffer();

        // Direction indicator
        if ( offset < 0 ) {
            out.append( '-' );
            offset *= -1;
        } else
            out.append( '+' );

        // Hours
        int hours = offset / MILLIS_PER_HOUR;
        if ( hours < 10 )
            out.append( '0' );
        out.append( hours );
        offset -= hours * MILLIS_PER_HOUR;

        out.append( ':' );

        // Minutes
        int mins = offset / MILLIS_PER_MINUTE;
        if ( mins < 10 )
            out.append( '0' );
        out.append( mins );

        return out.toString();
    }
    

    // User Specific data
    public static DateTimeZone getDateTimeZone( User user ) {
        return getDateTimeZone( getTimeZone( user ) );
    }

    public static TimeZone getTimeZone( User user ) {
        if ( user != null ) {
            return getUserAccessor().getConfluenceUserPreferences( user ).getTimeZone();
        }
        return TimeZone.getDefault();
    }

    private static UserAccessor getUserAccessor()
	{
		return CalendarManager.getInstance().getUserAccessor();
	}

    
	public static TimeZone getTimeZone( DateTimeZone timeZone ) {
        if ( timeZone != null )
            return TimeZone.getInstance( timeZone.getID() );
        return null;
    }

	public static LocalTime getDefaultStartTime(User user)
	{
		// TODO Auto-generated method stub
		return START_TIME_DEFAULT;
	}

	public static void setDefaultStartTime(User user, LocalTime startTime)
	{
		// TODO Auto-generated method stub
		
	}    
}
