/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.atlassian.confluence.extra.cal2.action;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.LocalTime;

import com.atlassian.confluence.extra.cal2.mgr.CalendarManager;
import com.atlassian.confluence.extra.cal2.mgr.TimeZoneUtils;
import com.atlassian.confluence.user.actions.AbstractUserProfileAction;
import com.opensymphony.util.TextUtils;
import com.opensymphony.webwork.ServletActionContext;

/**
 * Allows users to set their calendar preferences.
 * 
 * @author David Peterson
 */
public class EditMyCalendarSettingsAction extends AbstractUserProfileAction {
    private static final String TRUE = "true";

    private ResourceBundle resources;

    private String fromUrl;

    private String cancel;

    private int startHour;

    private int startMinute;

    public String execute() throws Exception {
        HttpServletRequest req = ServletActionContext.getRequest();

        if ( req != null && TRUE.equals( req.getParameter( "return" ) ) ) {
            fromUrl = getRefererURL( req );
        }

        setStartTime( TimeZoneUtils.getDefaultStartTime( getUser() ) );

        return SUCCESS;
    }

    public String doEdit() throws Exception {
        if ( TextUtils.stringSet( cancel ) ) {
            return CANCEL;
        } else {
        	TimeZoneUtils.setDefaultStartTime( getUser(), getStartTime() );
            return TextUtils.stringSet( fromUrl ) ? "return" : SUCCESS;
        }
    }

    public boolean isPermitted() {
        return getUser() != null && getUser().equals( getRemoteUser() );
    }

// TODO    
//    public ResourceBundle getResources() {
//        if ( resources == null )
//            resources = CalendarManager.getInstance().getResourceBundle( getRemoteUser() );
//        return resources;
//    }
//
//    public String getActionName( String className ) {
//        return getResources().getString( "calendar.title.settings" );
//    }

    /**
     * Returns the URL of the page which requested this page.
     * 
     * @param request
     *            The request object.
     * @return null if no valid url found.
     */
    public String getRefererURL( HttpServletRequest request ) {
        String referer = request.getHeader( "Referer" );

        if ( !TextUtils.stringSet( referer ) )
            return null;

        // If the referrer is "logout", make sure we don't set that as the URL
        // to return to!
        if ( referer.indexOf( "logout" ) > -1 || referer.indexOf( "login" ) > -1 )
            return null;

        return referer;
    }

    public String getFromUrl() {
        return fromUrl;
    }

    public void setFromUrl( String fromUrl ) {
        this.fromUrl = fromUrl;
    }

    public String getCancel() {
        return cancel;
    }

    public void setCancel( String cancel ) {
        this.cancel = cancel;
    }

    protected LocalTime getStartTime() {
        return new LocalTime( startHour, startMinute, 0, 0 );
    }

    protected void setStartTime( LocalTime startTime ) {
        if ( startTime != null ) {
            setStartHour( startTime.getHourOfDay() );
            setStartMinute( startTime.getMinuteOfHour() );
        } else {
            setStartHour( 0 );
            setStartMinute( 0 );
        }
    }

    public void setStartMinute( int minuteOfHour ) {
        startMinute = minuteOfHour;

    }

    public int getStartMinute() {
        return startMinute;
    }

    public void setStartHour( int hourOfDay ) {
        startHour = hourOfDay;
    }

    public int getStartHour() {
        return startHour;
    }

    public int getStartHour12() {
        int hour12 = startHour % 12;
        return ( hour12 == 0 ) ? 12 : hour12;
    }

    public void setStartHour12( int startHour12 ) {
        startHour12 = startHour12 % 12;
        startHour = ( startHour < 12 ) ? startHour12 : 12 + startHour12;
    }

    public boolean isStartAM() {
        return startHour < 12;
    }

    public void setStartAM( boolean startAM ) {
        if ( startAM && startHour >= 12 )
            startHour -= 12;
        else if ( !startAM && startHour < 12 )
            startHour += 12;
    }
}
