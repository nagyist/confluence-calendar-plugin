/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.cal2.action;

import java.nio.charset.Charset;

import org.randombits.source.Source;
import org.randombits.source.URLSource;
import org.randombits.source.confluence.AttachmentSource;

import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.cal2.display.CalendarDisplay;
import com.atlassian.confluence.extra.cal2.mgr.CalendarGroup;
import com.atlassian.confluence.extra.cal2.model.ICalCalendar;
import com.atlassian.confluence.extra.cal2.model.ICalendar;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;

/**
 * Provides methods and fields useful to actions dealing with individual
 * calendars
 */
public class CalendarHolderAction extends CalendarContainerAction
{

    /** The list of standard named colours */
    private static String[] COLORS = new String[] { "blue", "orange", "pink", "red", "green", "purple" };

    public static final String LOCAL_SOURCE = "local";
    public static final String REMOTE_SOURCE = "remote";
    public static final String ATTACHMENT_SOURCE = "attachment";

    // From BaseSubCalendar Action
    protected String name;
    protected String description;
    protected String color;

    // From AbstractICalCalendarAction
    protected String sourceType;

    protected String remoteSourceUrl;

    protected String connectionTimeout;

    protected String remoteSourceUsername;

    protected String remoteSourcePassword;

    protected boolean remoteSourceHasPassword;

    private ContentEntityManager contentEntityManager;

    protected String attachmentLink;

    protected String remoteSourceEncoding;

    protected int DEFAULT_TIMEOUT = 30;

    protected String subCalendarId;
    private ICalCalendar _subCalendar;

    public CalendarHolderAction()
    {
         super();

        // Default timeout
        setConnectionTimeout(Integer.toString(DEFAULT_TIMEOUT));
    }

    public String execute() throws Exception
    {
        return INPUT;
    }

    protected void setSubCalendar(ICalCalendar subCalendar)
    {
        this._subCalendar = subCalendar;
    }

    public ICalCalendar getSubCalendar()
    {
        if (_subCalendar == null && subCalendarId != null)
        {
        	// TODO  TEMP
            _subCalendar = (ICalCalendar) getCalendarManager().getCalenderHolder(subCalendarId);
        }
        
        return _subCalendar;
    }

    
    /**
     * Toggle the visibility of a calendar holder
     * 
     * @return
     */
    public String toggle() throws Exception {
		CalendarDisplay display = getCalendarDisplay();
        setDate(getDate());
        display.setDate(getDate());
        CalendarGroup calendar = getCalendar();
        ICalendar iCal = getSubCalendar();
        if (calendar.isUndisplayable(iCal) == true)
        {
            calendar.clearUndisplayable(iCal);
        }
        else
        {
            calendar.setUndisplayable(iCal);
        }
		return saveCalendar();
    }
    
    /*
     * Set up information about a specific calendar in the group
     */
    public String viewCalendar()
    {
        ICalCalendar cal = getSubCalendar();
        if (cal != null)
        {
            setName(cal.getName());
            setDescription(cal.getDescription());
            // TODO FIX
            setColor(cal.getColor());

            Source src = cal.getSource();
            if (src instanceof URLSource)
            {
                URLSource urlSrc = (URLSource) src;
                setSourceType(REMOTE_SOURCE);
                setRemoteSourceUrl(urlSrc.getUrl().toExternalForm());
                setConnectionTimeout(Integer.toString((int) urlSrc.getConnectionTimeout() / 1000));
                remoteSourceUsername = urlSrc.getUsername();
                remoteSourceHasPassword = urlSrc.hasPassword();
                Charset encoding = urlSrc.getEncoding();
                remoteSourceEncoding = encoding == null ? null : encoding.name();
            }
            else if (src instanceof AttachmentSource)
            {
                AttachmentSource attSrc = (AttachmentSource) src;
                setSourceType(ATTACHMENT_SOURCE);
                long contentId = attSrc.getContentId();
                attachmentLink = "";

                if (getContent() != null && contentId != getContent().getId())
                {
                    ContentEntityObject content = contentEntityManager.getById(contentId);
                    if (content instanceof Page)
                    {
                        attachmentLink = content.getTitle();
                    }
                    else if (content instanceof BlogPost)
                    {
                        attachmentLink = ((BlogPost) content).getDatePath() + content.getTitle();
                    }
                }

                attachmentLink += "^" + attSrc.getFileName();
            }
            else
            {
                setSourceType(LOCAL_SOURCE);
            }
        }
        else
        {
            setSourceType(LOCAL_SOURCE);
        }

        return INPUT;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getSubCalendarId()
    {
        return subCalendarId;
    }

    public void setSubCalendarId(String subCalendarId)
    {
        this.subCalendarId = subCalendarId;
    }

    public String[] getCalendarColors()
    {
        return COLORS;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public String getSourceType()
    {
        return sourceType;
    }

    public void setSourceType(String sourceType)
    {
        this.sourceType = sourceType;
    }

    public String getRemoteSourceUrl()
    {
        return remoteSourceUrl;
    }

    public void setRemoteSourceUrl(String remoteSourceUrl)
    {
        this.remoteSourceUrl = remoteSourceUrl;
    }

    public String getRemoteSourceUsername()
    {
        return remoteSourceUsername;
    }

    public void setRemoteSourceUsername(String remoteSourceUsername)
    {
        this.remoteSourceUsername = remoteSourceUsername;
    }

    public String getRemoteSourcePassword()
    {
        return getHashedPassword(remoteSourceHasPassword);
    }

    public void setRemoteSourcePassword(String remoteSourcePassword)
    {
        this.remoteSourcePassword = remoteSourcePassword;
    }

    private String getHashedPassword(boolean hasPassword)
    {
        if (hasPassword)
        {
            int chars = 6 + (int) (Math.random() * 10);
            StringBuffer out = new StringBuffer(chars);
            for (int i = 0; i < chars; i++)
                out.append('*');

            return out.toString();
        }
        return null;
    }

    public void setContentEntityManager(ContentEntityManager contentEntityManager)
    {
        this.contentEntityManager = contentEntityManager;
    }

    public String getAttachmentLink()
    {
        return attachmentLink;
    }

    public void setAttachmentLink(String attachmentLink)
    {
        this.attachmentLink = attachmentLink;
    }

    public String getRemoteSourceEncoding()
    {
        return remoteSourceEncoding;
    }

    public void setRemoteSourceEncoding(String remoteSourceEncoding)
    {
        this.remoteSourceEncoding = remoteSourceEncoding;
    }

    public String getConnectionTimeout()
    {
        return connectionTimeout;
    }

    public void setConnectionTimeout(String connectionTimeout)
    {
        this.connectionTimeout = connectionTimeout;
    }
}
