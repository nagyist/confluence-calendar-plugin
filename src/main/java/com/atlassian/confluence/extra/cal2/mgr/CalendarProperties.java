package com.atlassian.confluence.extra.cal2.mgr;

import java.util.Properties;

import net.fortuna.ical4j.util.CompatibilityHints;

public class CalendarProperties extends Properties
{
    public static final String RELAXED_PARSING_KEY = CompatibilityHints.KEY_RELAXED_PARSING;
    public static final String RELAXED_UNFOLDING_KEY = CompatibilityHints.KEY_RELAXED_UNFOLDING;
    public static final String RELAXED_VALIDATION_KEY = CompatibilityHints.KEY_RELAXED_VALIDATION;
    public static final String OUTLOOK_COMPATIBLE_KEY = CompatibilityHints.KEY_OUTLOOK_COMPATIBILITY;
    public static final String NOTES_COMPATIBLE_KEY = CompatibilityHints.KEY_NOTES_COMPATIBILITY;
    
    public CalendarProperties()
    {
        put(RELAXED_PARSING_KEY, "false");
        put(RELAXED_UNFOLDING_KEY, "false");
        put(RELAXED_VALIDATION_KEY, "false");
        put(OUTLOOK_COMPATIBLE_KEY, "false");
        put(NOTES_COMPATIBLE_KEY, "false");
    }
}
