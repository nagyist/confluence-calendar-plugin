package com.atlassian.confluence.extra.cal2.mgr;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

public class TestCalendarGroup extends TestCase
{
	private static final CalendarGroupKey TEST_GROUP_KEY = new CalendarGroupKey("space", "page", "group");

	
	public TestCalendarGroup(String name) {
		super(name);
	}
	
    // test default group contents (should be empty)
    public void testDefaultGroupContents() 
    {
    	CalendarGroup group = new CalendarGroup(TEST_GROUP_KEY);
    	assertNotNull(group.getMembers());
    	assertEquals(0, group.getMembers().size());
    }
	
    // test get hash key
    public void testGetHashKey()
    {
    	CalendarGroup group = new CalendarGroup(TEST_GROUP_KEY);
    	assertEquals( TEST_GROUP_KEY, group.getHashKey());
    }
    
    // test add/remove/get/contains members
    public void testAddGroupMembers()
    {
    	CalendarGroup group = new CalendarGroup(TEST_GROUP_KEY);

    	List<String> expected = new ArrayList<String>();
    	String id =  "id1";
    	expected.add(id);
    	group.add(id);
    	
    	id = "id2";
    	expected.add(id);
    	group.add(id);
    	
    	assertEquals(expected, group.getMembers());
    	
    	// Check for correctly handling duplicates
    	group.add(id);
    	assertEquals(expected, group.getMembers());
    }
    
    public void testRemoveGroupMembers()
    {
    	CalendarGroup group = new CalendarGroup(TEST_GROUP_KEY);

    	List<String> expected = new ArrayList<String>();
    	String id = "id1";
    	expected.add(id);
    	group.add(id);
    	
    	String idToRemove = "id2";
    	group.add(id);
    	
    	id = "id3";
    	expected.add(id);
    	group.add(id);
    	
    	group.remove(idToRemove);
    	
    	assertEquals(expected, group.getMembers());
    	
    }
    
    public void testContainsGroupMembers()
    {
    	CalendarGroup group = new CalendarGroup(TEST_GROUP_KEY);

    	String id1 = "id1";
    	group.add(id1);
    	
    	String id2 = "id2";
    	group.add(id2);
    	
    	String absentId = "id3";
    	
    	assertTrue(group.contains(id1));
    	assertTrue(group.contains(id2));
    	assertFalse(group.contains(absentId));
    }
}
